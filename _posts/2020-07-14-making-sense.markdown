---
layout: post
title:  "Making Sense"
date:   2020-07-14 15:50:00 +0800
categories: update
---

My mind is cluttered with so many thoughts. I guess this is a way for me to make sense of it all. The past few months has been quite a ride. Despite the very huge rough patch, there are many things to be thankful for:
- **I am grateful for the many people who have stuck around and the people who stepped up.** I have been the most stubborn piece of shit the past few years and I am sure I was not very pleasant to deal with for more reasons than one. I apologize it took so long for me to come to this realization. My view of reality was a little wonky. I do not say that to excuse any of my behaviors that hurt the people I supposedly care for but hope it earns a little bit of understanding. It is a long road to fixing the landslide that I have left behind. Once more, I ask for patience.
- **I am grateful that somebody showed me the way and taught me to respect myself.** For a while there, I thought I was just horrible, that I did not deserve respect or kindness, and that any of those that I received were owed. Man, that was rough.
- **I am grateful for family.** They probably don't fully understand what is going on or how it came to be but here they are.

Of course, leave it to me to decide to fix my life smack middle of a pandemic that is trying to ruin the lives of everyone around the world. Anyway...

#### What's this?
- **This is where I talk about things without actually talking.** I’m one of those people who talk a lot without actually talking, especially about feelings. At the same time, I still feel the need to say how I feel to no one in particular. It helps me understand my thoughts when I write them down.
- **A (maybe) regular log of how I feel.** So many things are happening at the same time. When someone asks, “Are you ok?” I find that, at times, it’s been a while since I thought about it. Thinking about it would need more time than is an acceptable length for a pause in a conversation. Usually, I end up just saying that I am ok. Having this log will hopefully help me be more honest with myself.

#### What's with the name?
"OK 200" is the [standard response for successful HTTP requests](https://tools.ietf.org/html/rfc2616#section-10.2.1). Sort of like when you ask someone (HTTP Request) “Are you ok?” and the standard response was (HTTP Response) “Yeah, I’m ok” but really, they just didn’t want to get into it.

#### Technical Notes
- This is a static site generated using [Jekyll](https://jekyllrb.com/). I might style it. I might not. Who knows? At least it’s easy to get a site up in 5 minutes. I wanted a blog that was less complicated than a full-fledged blog like Wordpress, Blogger, Tumblr, what-have-yous.
- It is hosted in [GitLab](https://gitlab.com/) and uses the pre-built CI/CD pipelines to automatically publish it in [GitLab Pages](https://about.gitlab.com/stages-devops-lifecycle/pages/). Since this is not a sponsored post, I should probably also mention that [GitHub](github.com) has [GitHub Pages](https://pages.github.com/) as well and Jekyll was originally made to work with it.
- Not that I expect anyone to contribute but [here's the project link](https://gitlab.com/ok200/ok200.gitlab.io) in case you're curious how to make one. Creating a project via the provided templates and following the guide shouldn't be too hard, though.
